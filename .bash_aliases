alias fiji='~/fiji/Fiji.app/ImageJ-linux64'
alias sau='sudo apt update'
alias fx='cd ~/java/java10/fracgen'
alias rfx='java -cp /media/john/sys2/imageJ/jars/ImageJ/ij.jar:. -Xmx8g FracGenWX -c8m -RR -! -H -C'
alias m='mkdir'
alias ffx='java -cp /media/john/sys2/imageJ/jars/ImageJ/ij.jar:. -Xmx8g FracGenWX -c6m -! -H -C'
alias tsdemo='ffx -f277 -g80'
alias cfx='javac -cp /media/john/sys2/imageJ/jars/ImageJ/ij.jar:. FracGenWX.java'
alias panel='xfce4-panel -p'
alias pg='cd /media/john/sys2/web18/playground'
alias disp=xfce4-display-settings
alias b=". ~/.bash_aliases"
alias ..="cd .."
alias ...="cd ../.."
alias p="ping -c 6 8.8.8.8"
alias sau='sudo apt update'
alias x='xdg-open'
alias gl='git log --oneline'
alias g=glslViewer
alias gf="wmctrl -r glslViewer -b add,fullscreen"
alias sib=".. && cd"
alias upg='sudo apt upgrade'
alias fupg='sudo apt full-upgrade'
alias sai='sudo apt install'
alias xr='xfce4-panel -r && xfwm4 --replace'
alias p5='cd /media/john/sys2/web18/code-institute/milestone-projects/project-5/'
